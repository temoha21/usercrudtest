<?php

namespace App\Models;

use App\Search\Searchable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $description
 * @property boolean $status
 *
 */
class User extends Authenticatable
{
    use Notifiable;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *array
     * @var
     */
    protected $fillable = [
        'name', 'email', 'description', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
