<?php

namespace App\Transformers;

use App\DTO\UserDefaultDTO;
use League\Fractal\TransformerAbstract;

class UserDefault extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserDefaultDTO $defaultDTO)
    {

        $baseFields = [
            'name' => $defaultDTO->getName(),
            'email' => $defaultDTO->getEmail(),
            'description' => $defaultDTO->getDescription(),
            'status' => $defaultDTO->getStatus()
        ];
        if ($defaultDTO->getId()) {
            $baseFields['id'] = $defaultDTO->getId();
        }
        return $baseFields;
    }
}
