<?php
/**
 * Created by PhpStorm.
 * User: temoh
 * Date: 20.03.2018
 * Time: 22:36
 */

namespace App\Services;


use App\DTO\UserDefaultDTO;
use App\Models\User;
use Illuminate\Support\Collection;

class UserService
{
    public function getAll () : Collection {
        return User::all()->map(function (User $user) {
            return new UserDefaultDTO(
                $user->name,
                $user->email,
                $user->description,
                $user->status
            );
        });
    }

    public function storeUser(UserDefaultDTO $defaultDTO) : bool
    {
        $user = new User([
            'name' => $defaultDTO->getName(),
            'email' => $defaultDTO->getEmail(),
            'description' => $defaultDTO->getDescription(),
            'status' => $defaultDTO->getStatus(),
        ]);
        return $user->save()? route('getUser').'/'.$user->id : false;
    }

    public function findById(int $id) : UserDefaultDTO
    {
        $user = User::query()->where('id', $id)->first();

        return new UserDefaultDTO(
            $user->name,
            $user->email,
            $user->description,
            $user->status,
            $user->id
            );
    }

    public function updateUser(UserDefaultDTO $defaultDTO)
    {
        $user = User::query()->where('id', $defaultDTO->getId())->first();

        $user->name = $defaultDTO->getName();
        $user->email = $defaultDTO->getEmail();
        $user->description = $defaultDTO->getDescription();
        $user->status = $defaultDTO->getStatus();

        return $user->save()? route('getUser').'/'.$user->id : false;
    }

    public function deleteUser($id)
    {
        return User::query()->where('id', $id)->delete();
    }

    public function search($query, $usersRepository)
    {
        $users = $usersRepository->search((string) request('q'));

        return $users->map(function (User $user) {
            return new UserDefaultDTO(
                $user->name,
                $user->email,
                $user->description,
                $user->status
            );
        });
    }
}