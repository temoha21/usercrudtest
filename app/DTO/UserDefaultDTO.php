<?php
/**
 * Created by PhpStorm.
 * User: temoh
 * Date: 22.03.2018
 * Time: 21:14
 */

namespace App\DTO;


class UserDefaultDTO
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $description;
    /**
     * @var bool
     */
    private $status;

    /**
     * UserDefaultDTO constructor.
     * @param int $id
     * @param string $name
     * @param string $email
     * @param string $description
     * @param bool $status
     */
    public function __construct(string $name, string $email, string $description, bool $status, int $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->description = $description;
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }




}