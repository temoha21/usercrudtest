<?php
/**
 * Created by PhpStorm.
 * User: temoh
 * Date: 26.03.2018
 * Time: 23:33
 */

namespace App\Users;

use Illuminate\Database\Eloquent\Collection;

interface UsersRepository
{
    public function search(string $query = ""): Collection;
}