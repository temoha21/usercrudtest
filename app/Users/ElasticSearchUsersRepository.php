<?php
/**
 * Created by PhpStorm.
 * User: temoh
 * Date: 26.03.2018
 * Time: 23:29
 */

namespace App\Users;


use Illuminate\Database\Eloquent\Collection;

class ElasticSearchUsersRepository
{
    private $search;

    public function __construct(Client $client) {
        $this->search = $client;
    }

    public function search(string $query = ""): Collection
    {
        $items = $this->searchOnElasticsearch($query);

        return $this->buildCollection($items);
    }

    private function searchOnElasticsearch(string $query): array
    {
        $instance = new User;

        $items = $this->search->search([
            'index' => $instance->getSearchIndex(),
            'type' => $instance->getSearchType(),
            'body' => [
                'query' => [
                    'multi_match' => [
                        'fields' => ['email', 'description', 'status'],
                        'query' => $query,
                    ],
                ],
            ],
        ]);

        return $items;
    }

    private function buildCollection(array $items): Collection
    {
        $hits = array_pluck($items['hits']['hits'], '_source') ?: [];

        return User::hydrate($hits);
    }
}