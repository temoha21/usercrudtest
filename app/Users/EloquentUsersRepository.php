<?php
/**
 * Created by PhpStorm.
 * User: temoh
 * Date: 26.03.2018
 * Time: 23:38
 */

namespace App\Users;


use Illuminate\Database\Eloquent\Collection;

class EloquentUsersRepository implements UsersRepository
{


    public function search(string $query = ""): Collection
    {
        return User::where('email', 'like', "%{$query}%")
            ->orWhere('description', 'like', "%{$query}%")
            ->orWhere('status', "%{$query}%")
            ->get();
    }
}