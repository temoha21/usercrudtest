<?php

namespace App\Http\Controllers;

use App\DTO\UserDefaultDTO;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Transformers\UserDefault;
use App\Users\UsersRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserService $userService
     * @return mixed
     */
    public function index(UserService $userService)
    {
        $users = $userService->getAll();
        return fractal($users ,new UserDefault())->respond();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStoreRequest $userStoreRequest
     * @param UserService $userService
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $userStoreRequest, UserService $userService)
    {
        $data = $userStoreRequest->only([
            'name',
            'email',
            'description',
            'status']
        );
        $creationUrl = $userService->storeUser(new UserDefaultDTO(
            $data['name'],
            $data['email'],
            $data['description'],
            $data['status']
        ));
        return $creationUrl? response($creationUrl, 201) : response('', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param UserService $userService
     * @return mixed
     */
    public function show(int $id, UserService $userService)
    {
        $userDTO = $userService->findById($id);

        return fractal($userDTO, new UserDefault())->respond();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $userUpdateRequest
     * @param  int $id
     * @param UserService $userService
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $userUpdateRequest, $id, UserService $userService)
    {
        $data = $userUpdateRequest->only([
                'name',
                'email',
                'description',
                'status']
        );
        $updateUrl = $userService->updateUser(new UserDefaultDTO(
            $data['name'],
            $data['email'],
            $data['description'],
            $data['status'],
            $id
        ));
        return $updateUrl? response($updateUrl, 204) : response('', 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param UserService $userService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserService $userService)
    {
        $status = $userService->deleteUser($id);
        return response('', $status? 200 : 400) ;
    }

    /**
     * @param UsersRepository $usersRepository
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(UsersRepository $usersRepository, UserService $userService){
        $users = $userService->search((string) \request('query'), $usersRepository);

        return fractal($users, new UserDefault())->respond();
    }
}
