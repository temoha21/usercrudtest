<?php

namespace App\Providers;

use App\Users\ElasticSearchUsersRepository;
use App\Users\EloquentUsersRepository;
use App\Users\UsersRepository;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UsersRepository::class, function($app) {
            if (!config('services.search.enabled')) {
                return new EloquentUsersRepository();
            }

            return new ElasticsearchUsersRepository(
                $app->make(Client::class)
            );
        });
    }
    private function bindSearchClient()
    {
        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts(config('services.search.hosts'))
                ->build();
        });
    }
}
