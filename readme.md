git clone --recursive
copy .env.docker-example to ./laradock folder
copy .env.example to .env
sh docker-compose-build.sh
sh docker-compose-up.sh